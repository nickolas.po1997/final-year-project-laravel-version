<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'auth',
    'namespace' => 'ApiAuth',
], function () {
    Route::post('login', 'JwtAuthController@login');
    Route::post('register', 'JwtAuthController@register');
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'JwtAuthController@logout');
//      Route::get('profile', 'JwtAuthController@profile');
    });
});

Route::group([
    'prefix' => 'user',
    'namespace' => 'User',
    'middleware' => 'auth:api'
], function () {
    Route::get('view', 'UserController@view');
    Route::post('update', 'UserController@update');
    Route::group([
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'auth.Admin'
    ], function () {
        Route::get('list/all', 'UserController@listAll');
        Route::get('view/{user}', 'UserController@view');
        Route::post('create', 'UserController@create');
        Route::post('update/{user}', 'UserController@update');
        Route::delete('delete/{user}', 'UserController@delete');
    });
});

Route::group([
    'prefix' => 'file',
    'namespace' => 'File',
    'middleware' => 'auth:api'
], function () {
    Route::group([
        'prefix' => 'image',
    ], function () {
        Route::post('store', 'FileController@storeImages');
        Route::delete('delete/{image}', 'FileController@deleteImage');
    });
});

Route::group([
    'prefix' => 'hotel',
    'namespace' => 'Hotel',
    'middleware' => 'auth:api'
], function () {
    Route::get('list/all', 'HotelController@listAll');
    Route::get('view/{hotel}', 'HotelController@view');
    Route::post('create', 'HotelController@create');
    Route::post('update/{hotel}', 'HotelController@update');
    Route::delete('delete/{hotel}', 'HotelController@delete');
});

Route::group([
    'prefix' => 'facility',
    'namespace' => 'Facility',
    'middleware' => 'auth:api'
], function () {
    Route::get('list/all', 'FacilityController@listAll');
    Route::post('create', 'FacilityController@create');
    Route::post('update/{facility}', 'FacilityController@update');
    Route::delete('delete/{facility}', 'FacilityController@delete');
});

Route::group([
    'prefix' => 'restaurant',
    'namespace' => 'Restaurant',
        'middleware' => 'auth:api'
], function () {
    Route::get('list/all', 'RestaurantController@listAll');
    Route::get('view/{restaurant}', 'RestaurantController@view');
    Route::post('create', 'RestaurantController@create');
    Route::post('update/{restaurant}', 'RestaurantController@update');
    Route::delete('delete/{restaurant}', 'RestaurantController@delete');
});

Route::group([
    'prefix' => 'attraction',
    'namespace' => 'Attraction',
        'middleware' => 'auth:api'
], function () {
    Route::get('list/all', 'AttractionController@listAll');
    Route::get('view/{attraction}', 'AttractionController@view');
    Route::post('create', 'AttractionController@create');
    Route::post('update/{attraction}', 'AttractionController@update');
    Route::delete('delete/{attraction}', 'AttractionController@delete');
});

Route::group([
    'prefix' => 'view',
    'namespace' => 'View',
        'middleware' => 'auth:api'
], function () {
    Route::get('list/all', 'RestaurantController@listAll');
    Route::get('view/{restaurant}', 'RestaurantController@view');
    Route::post('create', 'RestaurantController@create');
    Route::post('update/{restaurant}', 'RestaurantController@update');
    Route::delete('delete/{restaurant}', 'RestaurantController@delete');
});

Route::group([
    'prefix' => 'tag',
    'namespace' => 'Tag',
       'middleware' => 'auth:api'
], function () {
    Route::get('list/all', 'TagController@listAll');
    Route::get('view/{tag}', 'TagController@view');
    Route::post('search', 'TagController@search');
    Route::post('create', 'TagController@create');
    Route::delete('delete/{tag}', 'TagController@delete');
});

Route::group([
    'prefix' => 'plan',
    'namespace' => 'Plan',
    'middleware' => 'auth:api'
], function () {
    Route::get('list/all', 'PlanController@listAll');
    Route::get('view/{plan}', 'PlanController@view');
    Route::post('create', 'PlanController@create');
    Route::post('update/{plan}', 'PlanController@update');
    Route::delete('delete/{plan}', 'PlanController@delete');
    Route::group([
        'prefix' => 'itinerary',
        'namespace' => 'Itinerary',
//        'middleware' => ''
    ], function () {
        Route::get('view/{itinerary}', 'ItineraryController@view');
        Route::post('create/{plan}', 'ItineraryController@create');
        Route::post('update/order/{plan}', 'ItineraryController@order');
        Route::post('update/{itinerary}', 'ItineraryController@update');
        Route::delete('delete/{itinerary}', 'ItineraryController@delete');
    });
});

