<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Image;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker) {
    return [
        'path' => 'images/dc8SnvO8v1u2M5Ocu2KO2SmMovWe8s6jTX3DQrVX.jpeg',
        'url' => $faker->url,
    ];
});
