<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hotel;
use Faker\Generator as Faker;

$factory->define(Hotel::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'min_price' => $faker->numberBetween($min=1,$max=499),
        'max_price' => $faker->numberBetween($min=500,$max=999),
        'location' => $faker->streetAddress,
        'description' => $faker->text,
        'website' => $faker->url,
        'rating' => $faker->numberBetween($min=1,$max=5)
    ];
});
