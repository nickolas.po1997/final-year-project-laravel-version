<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Restaurant;
use Faker\Generator as Faker;

$factory->define(Restaurant::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'min_price' => $faker->numberBetween($min = 1, $max = 499),
        'location' => $faker->streetAddress,
        'phone' => $faker->phoneNumber,
        'website' => $faker->url
    ];
});
