<?php

use App\Attraction;
use Illuminate\Database\Seeder;

class AttractionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Attraction::truncate();
        factory(Attraction::class, 20)->create();
    }
}
