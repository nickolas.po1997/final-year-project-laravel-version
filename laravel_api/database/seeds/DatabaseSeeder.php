<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(HotelsTableSeeder::class);
        $this->call(FacilitiesTableSeeder::class);
        $this->call(RestaurantsTableSeeder::class);
        $this->call(AttractionsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(ImagesTableSeeder::class);
    }
}
