<?php


namespace App\Facade;


class JsonResponseFacade
{

    public function error($message = "404 Not Found", $statusCode = 404)
    {
        return response()->json([
            "message" => $message,
        ], $statusCode);
    }

    public function response($data, $statusCode, $isToken)
    {
        if ($data == null) {
            return $this->responseWithNoData($statusCode);
        } else {
            if ($isToken) {
                return $this->responseWithToken($data, $statusCode);
            } else {
                return $this->responseWithData($data, $statusCode);
            }
        }
    }

    private function responseWithData($data, $statusCode)
    {
        return response()->json([
            "statusCode" => $statusCode,
            "data" => $data
        ], $statusCode);
    }

    private function responseWithNoData($statusCode)
    {
        return response()->json([
            "statusCode" => $statusCode,
        ], $statusCode);
    }

    private function responseWithToken($token, $statusCode)
    {
        return response()->json([
            "statusCode" => $statusCode,
            "token" => $token
        ], $statusCode);
    }

}
