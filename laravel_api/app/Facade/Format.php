<?php


namespace App\Facade;


class Format
{

    protected static function resolveFacade($name)
    {
        return app()[$name];
    }

    public static function __callStatic($method, $arguments)
    {
        return (self::resolveFacade('Format'))
            ->$method(...$arguments);
    }

}
