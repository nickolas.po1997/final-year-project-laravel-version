<?php


namespace App\Repository\FacilityRepository;


use App\Facade\Format;
use App\Facility;

class FacilityRepositoryImpl implements FacilityRepository
{

    public function findById($id)
    {
        return Facility::find($id);
    }

    public function findAllWithPageable()
    {
        return Format::paginate(Facility::all());
    }

    public function findByFieldValuePair($field, $value)
    {
        return Facility::where($field, $value)->get();
    }

    public function create($data)
    {
        return Facility::create($data);
    }

    public function update($data, $target)
    {
        return $target->update($data);
    }

    public function delete($target)
    {
        return $target->delete();
    }


}
