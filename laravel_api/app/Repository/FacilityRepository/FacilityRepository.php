<?php


namespace App\Repository\FacilityRepository;


interface FacilityRepository
{

    public function findById($id);

    public function findAllWithPageable();

    public function findByFieldValuePair($field, $value);

    public function create($data);

    public function update($data, $target);

    public function delete($target);

}
