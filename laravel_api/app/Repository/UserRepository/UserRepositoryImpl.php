<?php


namespace App\Repository\UserRepository;


use App\Facade\Format;
use App\User;

class UserRepositoryImpl implements UserRepository
{

    private $relationship = ['roles'];

    public function findAllWithPageable()
    {
        return Format::paginate(User::all());
    }

    public function loadTarget($target)
    {
        return User::find($target->id)->load($this->relationship);
    }

    public function findByFieldValuePair($field, $value)
    {
        return User::where($field, $value)->get();
    }

    public function findByQuery($query)
    {
        foreach ($query as $col => $val) {

        }
    }

    public function create($data)
    {
        return User::create($data);
    }

    public function update($data, $target)
    {
        return $target->update($data);
    }

    public function delete($target)
    {
        return $target->delete();
    }

}
