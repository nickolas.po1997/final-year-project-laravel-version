<?php


namespace App\Repository\UserRepository;


interface UserRepository
{
    public function findAllWithPageable();

    public function loadTarget($target);

    public function findByFieldValuePair($field, $value);

    public function findByQuery($query);

    public function create($data);

    public function update($data, $target);

    public function delete($target);
}
