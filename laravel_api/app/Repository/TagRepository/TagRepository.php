<?php


namespace App\Repository\TagRepository;


interface TagRepository
{
    public function loadTarget($target);

    public function findAllWithPageable();

    public function findByFieldValuePair($field, $value);

    public function searchByName($target);

    public function create($data);

    public function delete($target);
}
