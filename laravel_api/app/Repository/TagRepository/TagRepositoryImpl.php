<?php


namespace App\Repository\TagRepository;


use App\Facade\Format;
use App\Tag;

class TagRepositoryImpl implements TagRepository
{

    private $relationship = ['hotels','restaurants','attractions','plans'];

    public function loadTarget($target)
    {
        return Tag::find($target->id)->load($this->relationship);
    }

    public function findAllWithPageable()
    {
        return Format::paginate(Tag::all()->load($this->relationship));
    }

    public function findByFieldValuePair($field, $value)
    {
        return Tag::where($field, $value)->get();
    }

    public function searchByName($target)
    {
        return Tag::where('name', 'like', '%' . $target . '%')
            ->limit(20)->get();
    }

    public function create($data)
    {
        return Tag::create($data);
    }

    public function delete($target)
    {
        return $target->delete();
    }

}
