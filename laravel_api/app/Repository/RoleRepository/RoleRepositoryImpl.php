<?php


namespace App\Repository\RoleRepository;


use App\Role;

class RoleRepositoryImpl implements RoleRepository
{

    public function findByName($name)
    {
        return Role::where('name', $name)->first();
    }

}
