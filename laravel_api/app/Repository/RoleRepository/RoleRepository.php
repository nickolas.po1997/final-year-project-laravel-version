<?php


namespace App\Repository\RoleRepository;


interface RoleRepository
{
    public function findByName($name);
}
