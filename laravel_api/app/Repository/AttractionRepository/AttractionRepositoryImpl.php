<?php


namespace App\Repository\AttractionRepository;


use App\Attraction;
use App\Facade\Format;

class AttractionRepositoryImpl implements AttractionRepository
{

    private $relationship = ['images', 'tags'];

    public function findById($id)
    {
        return Attraction::find($id);
    }

    public function findAllWithPageable()
    {
        return Format::paginate(Attraction::all()->load($this->relationship));
    }

    public function findByFieldValuePair($field, $value)
    {
        return Attraction::where($field, $value)->get();
    }

    public function loadTarget($target)
    {
        return Attraction::find($target->id)->load($this->relationship);
    }

    public function create($data)
    {
        return Attraction::create($data);
    }

    public function update($data, $target)
    {
        return $target->update($data);
    }

    public function delete($target)
    {
        return $target->delete();
    }
}
