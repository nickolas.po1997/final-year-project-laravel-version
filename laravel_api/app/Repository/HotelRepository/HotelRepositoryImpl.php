<?php


namespace App\Repository\HotelRepository;


use App\Facade\Format;
use App\Hotel;

class HotelRepositoryImpl implements HotelRepository
{

    private $relationship = ['facilities', 'images', 'tags'];

    public function findById($id)
    {
        return Hotel::find($id);
    }

    public function findAllWithPageable()
    {
        return Format::paginate(Hotel::all()->load($this->relationship));
    }

    public function findByFieldValuePair($field, $value)
    {
        return Hotel::where($field, $value)->get();
    }

    public function loadTarget($target)
    {
        return Hotel::find($target->id)->load($this->relationship);
    }

    public function create($data)
    {
        return Hotel::create($data);
    }

    public function update($data, $target)
    {
        return $target->update($data);
    }

    public function delete($target)
    {
        return $target->delete();
    }

}
