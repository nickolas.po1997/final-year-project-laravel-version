<?php


namespace App\Repository\PlanRepository;


use App\Facade\Format;
use App\Plan;

class PlanRepositoryImpl implements PlanRepository
{

    private $relationship = ['user','tags'];

    public function findAllWithPageable()
    {
        return Format::paginate(Plan::all());
    }

    public function loadTarget($target)
    {
        return Plan::find($target->id)->load($this->relationship);
    }

    public function create($data, $user)
    {
        return $user->plans()->create($data);
    }

    public function update($data, $target)
    {
        return $target->update($data);
    }

    public function delete($target)
    {
        return $target->delete();
    }

}
