<?php


namespace App\Repository\PlanRepository;


interface PlanRepository
{
    public function findAllWithPageable();

    public function loadTarget($target);

    public function create($data, $user);

    public function update($data, $target);

    public function delete($target);
}
