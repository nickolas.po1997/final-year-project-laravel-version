<?php


namespace App\Repository\ItineraryRepository;


use App\Itinerary;

class ItineraryRepositoryImpl implements ItineraryRepository
{

    private $relationship = ['itineraryable'];

    public function list($plan)
    {
        return $plan->itineraries->map(function ($itinerary) {
            return $itinerary->load($this->relationship);
        });
    }

    public function loadTarget($target)
    {
        return $target->load($this->relationship);
    }

    public function findById($id)
    {
        return Itinerary::find($id);
    }

    public function create($data, $plan, $itineraryable)
    {
        $itinerary = $itineraryable->itineraries()->create($data);
        $itinerary->plan()->associate($plan);
        return $itinerary->save();
    }

    public function update($data, $target)
    {
        return $target->update($data);
    }

    public function delete($target)
    {
        return $target->delete();
    }


}
