<?php


namespace App\Repository\ItineraryRepository;


interface ItineraryRepository
{
    public function list($plan);

    public function loadTarget($target);

    public function findById($id);

    public function create($data, $plan, $itineraryable);

    public function update($data, $target);

    public function delete($target);
}
