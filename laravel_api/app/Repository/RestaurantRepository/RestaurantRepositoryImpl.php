<?php


namespace App\Repository\RestaurantRepository;


use App\Facade\Format;
use App\Restaurant;

class RestaurantRepositoryImpl implements RestaurantRepository
{

    private $relationship = ['images', 'tags'];

    public function findById($id)
    {
        return Restaurant::find($id);
    }

    public function findAllWithPageable()
    {
        return Format::paginate(Restaurant::all()->load($this->relationship));
    }

    public function loadTarget($target)
    {
        return Restaurant::find($target->id)->load($this->relationship);
    }

    public function findByFieldValuePair($field, $value)
    {
        return Restaurant::where($field, $value)->get();
    }

    public function create($data)
    {
        return Restaurant::create($data);
    }

    public function update($data, $target)
    {
        return $target->update($data);
    }

    public function delete($target)
    {
        return $target->delete();
    }

}
