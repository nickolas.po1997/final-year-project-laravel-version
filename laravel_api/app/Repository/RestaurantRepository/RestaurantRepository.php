<?php


namespace App\Repository\RestaurantRepository;


interface RestaurantRepository
{
    public function findById($id);

    public function findAllWithPageable();

    public function findByFieldValuePair($field, $value);

    public function loadTarget($target);

    public function create($data);

    public function update($data, $target);

    public function delete($target);
}
