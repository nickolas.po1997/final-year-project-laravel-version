<?php


namespace App\Repository\ImageRepository;


interface ImageRepository
{
    public function save($data);

    public function findById($id);

    public function delete($target);
}
