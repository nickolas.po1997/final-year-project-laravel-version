<?php


namespace App\Repository\ImageRepository;


use App\Image;

class ImageRepositoryImpl implements ImageRepository
{
    public function save($data)
    {
        return Image::create($data);
    }

    public function findById($id)
    {
        return Image::find($id);
    }

    public function delete($target)
    {
        return $target->delete();
    }
}
