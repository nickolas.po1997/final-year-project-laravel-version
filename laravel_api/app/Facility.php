<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facility extends Model
{

    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = ["pivot"];

    public function hotels()
    {
        return $this->belongsToMany(Hotel::class);
    }
}
