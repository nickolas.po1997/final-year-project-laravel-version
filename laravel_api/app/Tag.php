<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;

    protected $fillable = ["name"];

    public function hotels()
    {
        return $this->morphedByMany(Hotel::class, 'taggable');
    }

    public function restaurants()
    {
        return $this->morphedByMany(Restaurant::class, 'taggable');
    }

    public function attractions()
    {
        return $this->morphedByMany(Attraction::class, 'taggable');
    }

    public function plans()
    {
        return $this->morphedByMany(Plan::class, 'taggable');
    }

}
