<?php

namespace App\Providers;

use App\Service\JwtAuthService\JwtAuthService;
use App\Service\JwtAuthService\JwtAuthServiceImpl;
use Illuminate\Support\ServiceProvider;

class JwtAuthServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(JwtAuthService::class, JwtAuthServiceImpl::class);
    }
}
