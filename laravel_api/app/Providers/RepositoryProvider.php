<?php

namespace App\Providers;

use App\Repository\AttractionRepository\AttractionRepository;
use App\Repository\AttractionRepository\AttractionRepositoryImpl;
use App\Repository\FacilityRepository\FacilityRepository;
use App\Repository\FacilityRepository\FacilityRepositoryImpl;
use App\Repository\HotelRepository\HotelRepository;
use App\Repository\HotelRepository\HotelRepositoryImpl;
use App\Repository\ImageRepository\ImageRepository;
use App\Repository\ImageRepository\ImageRepositoryImpl;
use App\Repository\ItineraryRepository\ItineraryRepository;
use App\Repository\ItineraryRepository\ItineraryRepositoryImpl;
use App\Repository\PlanRepository\PlanRepository;
use App\Repository\PlanRepository\PlanRepositoryImpl;
use App\Repository\RestaurantRepository\RestaurantRepository;
use App\Repository\RestaurantRepository\RestaurantRepositoryImpl;
use App\Repository\RoleRepository\RoleRepository;
use App\Repository\RoleRepository\RoleRepositoryImpl;
use App\Repository\TagRepository\TagRepository;
use App\Repository\TagRepository\TagRepositoryImpl;
use App\Repository\UserRepository\UserRepository;
use App\Repository\UserRepository\UserRepositoryImpl;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(UserRepository::class, UserRepositoryImpl::class);
        $this->app->bind(RoleRepository::class, RoleRepositoryImpl::class);

        $this->app->bind(HotelRepository::class, HotelRepositoryImpl::class);
        $this->app->bind(FacilityRepository::class, FacilityRepositoryImpl::class);
        $this->app->bind(RestaurantRepository::class, RestaurantRepositoryImpl::class);
        $this->app->bind(AttractionRepository::class, AttractionRepositoryImpl::class);

        $this->app->bind(PlanRepository::class, PlanRepositoryImpl::class);
        $this->app->bind(ItineraryRepository::class, ItineraryRepositoryImpl::class);

        $this->app->bind(ImageRepository::class, ImageRepositoryImpl::class);
        $this->app->bind(TagRepository::class, TagRepositoryImpl::class);
    }
}
