<?php

namespace App\Providers;

use App\Facade\FormatFacade;
use App\Facade\JsonResponseFacade;
use App\Facade\ValidateFacade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Singleton Json Response Facade
        $this->app->singleton('JsonResponse', function ($app) {
            return new JsonResponseFacade();
        });
        // Singleton Format Facade
        $this->app->singleton('Format', function ($app) {
            return new FormatFacade();
        });
    }
}
