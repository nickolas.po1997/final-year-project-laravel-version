<?php

namespace App\Providers;

use App\Service\AttractionService\AttractionService;
use App\Service\AttractionService\AttractionServiceImpl;
use App\Service\FacilityService\FacilityService;
use App\Service\FacilityService\FacilityServiceImpl;
use App\Service\HotelService\HotelService;
use App\Service\HotelService\HotelServiceImpl;
use App\Service\ImageService\ImageService;
use App\Service\ImageService\ImageServiceImpl;
use App\Service\ItineraryService\ItineraryService;
use App\Service\ItineraryService\ItineraryServiceImpl;
use App\Service\PlanService\PlanService;
use App\Service\PlanService\PlanServiceImpl;
use App\Service\RestaurantService\RestaurantService;
use App\Service\RestaurantService\RestaurantServiceImpl;
use App\Service\TagService\TagService;
use App\Service\TagService\TagServiceImpl;
use App\Service\UserService\UserService;
use App\Service\UserService\UserServiceImpl;
use App\Service\ValidateService\ValidateService;
use App\Service\ValidateService\ValidateServiceImpl;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ValidateService::class, ValidateServiceImpl::class);

        $this->app->bind(UserService::class, UserServiceImpl::class);

        $this->app->bind(HotelService::class, HotelServiceImpl::class);
        $this->app->bind(FacilityService::class, FacilityServiceImpl::class);
        $this->app->bind(RestaurantService::class, RestaurantServiceImpl::class);
        $this->app->bind(AttractionService::class, AttractionServiceImpl::class);

        $this->app->bind(PlanService::class, PlanServiceImpl::class);
        $this->app->bind(ItineraryService::class, ItineraryServiceImpl::class);

        $this->app->bind(ImageService::class, ImageServiceImpl::class);
        $this->app->bind(TagService::class, TagServiceImpl::class);
    }
}
