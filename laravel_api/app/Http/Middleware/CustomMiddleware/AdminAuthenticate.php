<?php

namespace App\Http\Middleware\CustomerMiddleware;

use App\Http\Facade\JsonResponse;
use Closure;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->hasAnyRole('sysadmin')){
            return $next($request);
        }
        return JsonResponse::error("Unauthorized", 401, false);
    }
}
