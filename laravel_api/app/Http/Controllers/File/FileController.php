<?php

namespace App\Http\Controllers\File;

use App\Facade\JsonResponse;
use App\Http\Controllers\Controller;
use App\Service\ImageService\ImageService;
use App\Image;
use Illuminate\Support\Facades\Validator;

class FileController extends Controller
{

    private $imageService;

    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function storeImages()
    {
        $data = $this->validate(request(), [
            'images' => 'array|between:0,10)',
            'images.*' => 'mimes:png,jpeg,jpg|max:1000000'
        ]);
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        return JsonResponse::response($this->imageService->save(), 201, false);
    }

    public function deleteImage(Image $image){
        return JsonResponse::response($this->imageService->delete($image), 200, false);
    }

    private function getRequestData()
    {
        return request([
            'images',
        ]);
    }

    private function makeValidator($data)
    {
        $rule = [
            'images' => 'array|between:0,10)',
            'images.*' => 'mimes:png,jpeg,jpg|max:1000000'
        ];
        return Validator::make($data, $rule);
    }


}
