<?php


namespace App\Http\Controllers\Plan\Itinerary;


use App\Facade\JsonResponse;
use App\Http\Controllers\Controller;
use App\Itinerary;
use App\Plan;
use App\Service\ItineraryService\ItineraryService;
use App\Service\ValidateService\ValidateService;
use Illuminate\Support\Facades\Validator;

class ItineraryController extends Controller
{
    private $itineraryService;
    private $validateService;

    public function __construct(ItineraryService $itineraryService, ValidateService $validateService)
    {
        $this->itineraryService = $itineraryService;
        $this->validateService = $validateService;
    }

    public function view(Itinerary $itinerary)
    {
        return JsonResponse::response($this->itineraryService->viewInfo($itinerary), 200, false);
    }

    public function create(Plan $plan)
    {
        $rule = [
            'description' => 'required|min:1',
            'itineraryable_id' => 'required|numeric',
            'itineraryable_type' => 'required|regex:/^[HRA]$/',
        ];
        $input = ['description', 'itineraryable_id', 'itineraryable_type'];
        $data = $this->getRequestData($input);
        $validation = $this->makeValidator($data, $rule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        return JsonResponse::response($this->itineraryService->create($data, $plan), 200, false);
    }

    public function update(Itinerary $itinerary)
    {
        $rule = [
            'description' => 'required|min:1',
        ];
        $input = ['description'];
        $data = $this->getRequestData($input);
        $validation = $this->makeValidator($data, $rule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        return JsonResponse::response($this->itineraryService->update($data, $itinerary), 200, false);
    }


    public function order(Plan $plan)
    {
        $rule = [
            'order_list' => 'array|required',
            'order_list.*' => 'numeric|exists:itineraries,id'
        ];
        $input = ['order_list'];
        $data = $this->getRequestData($input);
        $validation = $this->makeValidator($data, $rule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->itineraryService, $plan);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->itineraryService->order($data, $plan), 200, false);
    }

    public function delete(Itinerary $itinerary)
    {
        return JsonResponse::response($this->itineraryService->delete($itinerary), 200, false);
    }

    private function getRequestData($input)
    {
        return request($input);
    }

    private function makeValidator($data, $rule)
    {
        return Validator::make($data, $rule);
    }

}
