<?php


namespace App\Http\Controllers\Plan;


use App\Facade\JsonResponse;
use App\Http\Controllers\Controller;
use App\Plan;
use App\Service\ItineraryService\ItineraryService;
use App\Service\PlanService\PlanService;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller
{

    private $baseRule = [
        'title' => 'required|min:1',
        'scale' => 'required|min:1|numeric',
        'tag_list' => 'array',
        'tag_list.*' => 'numeric|exists:tags,id'
    ];
    private $planService;
    private $itineraryService;

    public function __construct(PlanService $planService, ItineraryService $itineraryService)
    {
        $this->planService = $planService;
        $this->itineraryService = $itineraryService;
    }

    public function listAll()
    {
        return JsonResponse::response(
            $this->planService->findAllWithPageable(), 200, false
        );
    }

    public function view(Plan $plan)
    {
        return JsonResponse::response($this->planService->viewInfo($plan), 200, false);
    }

    public function create()
    {
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        return JsonResponse::response($this->planService->create($data), 200, false);
    }

    public function update(Plan $plan)
    {
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        return JsonResponse::response($this->planService->update($data, $plan), 200, false);
    }

    public function delete(Plan $plan)
    {
        return JsonResponse::response($this->planService->delete($plan), 200, false);
    }

    private function getRequestData()
    {
        return request([
            'title', 'scale', 'tag_list'
        ]);
    }

    private function makeValidator($data)
    {
        return Validator::make($data, $this->baseRule);
    }

}
