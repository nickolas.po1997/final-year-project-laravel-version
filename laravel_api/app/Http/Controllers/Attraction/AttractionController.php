<?php

namespace App\Http\Controllers\Attraction;

use App\Attraction;
use App\Facade\JsonResponse;
use App\Facade\Validate;
use App\Http\Controllers\Controller;
use App\Service\AttractionService\AttractionService;
use App\Service\ValidateService\ValidateService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class AttractionController extends Controller
{

    private $baseRule = [
        'overview' => 'required',
        'location' => 'required',
        'image_list' => 'array',
        'image_list.*' => 'numeric|exists:images,id',
        'tag_list' => 'array',
        'tag_list.*' => 'numeric|exists:tags,id'
    ];
    private $attractionService;
    private $validateService;

    public function __construct(AttractionService $attractionService, ValidateService $validateService)
    {
        $this->attractionService = $attractionService;
        $this->validateService = $validateService;
    }

    public function listAll()
    {
        return JsonResponse::response(
            $this->attractionService->findAllWithPageable(), 200, false
        );
    }

    public function view(Attraction $attraction)
    {
        return JsonResponse::response($this->attractionService->viewInfo($attraction), 200, false);
    }

    public function create()
    {
        $additionRule = [
            'name' => 'required|min:3|unique:attractions',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $additionRule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->attractionService, null);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->attractionService->create($data), 201, false);

    }

    public function update(Attraction $attraction)
    {
        $additionRule = [
            'name' => 'required|min:3',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $additionRule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->attractionService, $attraction);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->attractionService->update($data, $attraction), 200, false);
    }

    public function delete(Attraction $attraction)
    {
        return JsonResponse::response($this->attractionService->delete($attraction), 200, false);
    }

    private function getRequestData()
    {
        return request([
            'name', 'overview', 'location', 'image_list', 'tag_list'
        ]);
    }

    private function makeValidator($data, $additionRule)
    {
        $rule = Arr::collapse([$additionRule, $this->baseRule]);
        return Validator::make($data, $rule);
    }


}
