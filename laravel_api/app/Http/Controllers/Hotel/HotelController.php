<?php

namespace App\Http\Controllers\Hotel;

use App\Facade\JsonResponse;
use App\Facade\Validate;
use App\Hotel;
use App\Http\Controllers\Controller;
use App\Service\HotelService\HotelService;
use App\Service\ValidateService\ValidateService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class HotelController extends Controller
{

    private $baseRule = [
        'min_price' => 'required|min:0|numeric',
        'max_price' => 'required|numeric',
        'location' => 'required|',
        'description' => 'max:1024',
        'website' => 'required|url',
        'rating' => 'required|max:5|min:1|numeric',
        'facility_list' => 'array',
        'facility_list.*' => 'numeric|exists:facilities,id',
        'image_list' => 'array',
        'image_list.*' => 'numeric|exists:images,id',
        'tag_list' => 'array',
        'tag_list.*' => 'numeric|exists:tags,id'
    ];

    private $hotelService;
    private $validateService;

    public function __construct(HotelService $hotelService, ValidateService $validateService)
    {
        $this->hotelService = $hotelService;
        $this->validateService = $validateService;
    }

    public function listAll()
    {
        return JsonResponse::response(
            $this->hotelService->findAllWithPageable(), 200, false
        );
    }

    public function view(Hotel $hotel)
    {
        return JsonResponse::response($this->hotelService->viewInfo($hotel), 200, false);
    }

    public function create()
    {
        $additionRule = [
            'name' => 'required|min:3|unique:hotels',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $additionRule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->hotelService, null);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->hotelService->create($data), 201, false);
    }

    public function update(Hotel $hotel)
    {
        $additionRule = [
            'name' => 'required|min:3',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $additionRule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->hotelService, $hotel);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->hotelService->update($data, $hotel), 200, false);
    }

    public function delete(Hotel $hotel)
    {
        return JsonResponse::response($this->hotelService->delete($hotel), 200, false);
    }

    private function getRequestData()
    {
        return request([
            'name', 'min_price', 'max_price', 'location',
            'description', 'website', 'rating', 'facility_list', 'image_list',
            'tag_list'
        ]);
    }

    private function makeValidator($data, $additionRule)
    {
        $rule = Arr::collapse([$additionRule, $this->baseRule]);
        return Validator::make($data, $rule);
    }

}
