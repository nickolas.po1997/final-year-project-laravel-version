<?php

namespace App\Http\Controllers\ApiAuth;

use App\Facade\JsonResponse;
use App\Http\Controllers\Controller;
use App\Service\JwtAuthService\JwtAuthService;
use Illuminate\Support\Facades\Validator;

class JwtAuthController extends Controller
{

    private $jwtAuthService;

    public function __construct(JwtAuthService $jwtAuthService)
    {
        $this->jwtAuthService = $jwtAuthService;
    }

    public function login()
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ];
        $data = request(['email', 'password']);
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        return $this->jwtAuthService->login($data);
    }

    public function register()
    {
        $rules = [
            'name' => 'required|min:3|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'gender' => array('required', 'regex:/^(Male|Female)$/'),
            'birthday' => 'required|date_format:Y-m-d'
        ];
        $data = request(['name', 'email', 'password', 'gender', 'birthday']);
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        return $this->jwtAuthService->register($data);
    }

    public function profile(){
        return $this->jwtAuthService->me();
    }

    public function logout()
    {
        return $this->jwtAuthService->logout();
    }

    public function refresh()
    {
        return $this->jwtAuthService->refresh();
    }

}
