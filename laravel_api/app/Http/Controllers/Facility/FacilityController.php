<?php

namespace App\Http\Controllers\Facility;

use App\Facade\JsonResponse;
use App\Facade\Validate;
use App\Facility;
use App\Http\Controllers\Controller;
use App\Service\FacilityService\FacilityService;
use App\Service\ValidateService\ValidateService;
use Illuminate\Support\Facades\Validator;

class FacilityController extends Controller
{

    private $facilityService;
    private $validateService;

    public function __construct(FacilityService $facilityService, ValidateService $validateService)
    {
        $this->facilityService = $facilityService;
        $this->validateService = $validateService;

    }

    public function listAll()
    {
        return JsonResponse::response(
            $this->facilityService->findAllWithPageable(), 200, false
        );
    }

    public function create()
    {
        $rule = [
            'name' => 'required|min:3|unique:facilities',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $rule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->facilityService, null);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->facilityService->create($data), 201, false);
    }

    public function update(Facility $facility)
    {
        $rule = [
            'name' => 'required|min:3',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $rule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->facilityService, $facility);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->facilityService->update($data, $facility), 200, false);
    }

    public function delete(Facility $facility)
    {
        return JsonResponse::response($this->facilityService->delete($facility), 200, false);
    }

    private function getRequestData()
    {
        return request(['name']);
    }

    private function makeValidator($data, $rule)
    {
        return Validator::make($data, $rule);
    }


}
