<?php

namespace App\Http\Controllers\Restaurant;

use App\Facade\JsonResponse;
use App\Facade\Validate;
use App\Http\Controllers\Controller;
use App\Restaurant;
use App\Service\RestaurantService\RestaurantService;
use App\Service\ValidateService\ValidateService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class RestaurantController extends Controller
{

    private $baseRule = [
        'min_price' => 'required|min:0|numeric',
        'location' => 'required',
        'phone' => 'required|regex:/^([+]\d{2,3}[- ])?\d{8}$/',
        'website' => 'required|url',
        'image_list' => 'array',
        'image_list.*' => 'numeric|exists:images,id',
        'tag_list' => 'array',
        'tag_list.*' => 'numeric|exists:tags,id'
    ];

    private $restaurantService;
    private $validateService;

    public function __construct(RestaurantService $restaurantService, ValidateService $validateService)
    {
        $this->restaurantService = $restaurantService;
        $this->validateService = $validateService;
    }

    public function listAll()
    {
        return JsonResponse::response(
            $this->restaurantService->findAllWithPageable(), 200, false
        );
    }

    public function view(Restaurant $restaurant)
    {
        return JsonResponse::response($this->restaurantService->viewInfo($restaurant), 200, false);
    }

    public function create()
    {
        $additionRule = [
            'name' => 'required|min:3|unique:restaurants',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $additionRule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->restaurantService, null);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->restaurantService->create($data), 201, false);
    }

    public function update(Restaurant $restaurant)
    {
        $additionRule = [
            'name' => 'required|min:3',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $additionRule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->restaurantService, $restaurant);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->restaurantService->update($data, $restaurant), 200, false);
    }

    public function delete(Restaurant $restaurant)
    {
        return JsonResponse::response($this->restaurantService->delete($restaurant), 200, false);
    }

    private function getRequestData()
    {
        return request([
            'name', 'min_price', 'location', 'phone',
            'website'
        ]);
    }

    private function makeValidator($data, $additionRule)
    {
        $rule = Arr::collapse([$additionRule, $this->baseRule]);
        return Validator::make($data, $rule);
    }

}
