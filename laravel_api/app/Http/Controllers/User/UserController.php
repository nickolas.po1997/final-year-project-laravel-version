<?php


namespace App\Http\Controllers\User;


use App\Facade\JsonResponse;
use App\Service\UserService\UserService;
use App\Service\ValidateService\ValidateService;
use Illuminate\Support\Facades\Validator;

class UserController
{

    private $userService;
    private $validateService;

    public function __construct(UserService $userService, ValidateService $validateService)
    {
        $this->userService = $userService;
        $this->validateService = $validateService;
    }

    public function view()
    {
        return JsonResponse::response($this->userService->viewInfo(auth()->user()), 200, false);
    }

    public function update()
    {
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->userService, auth()->user());
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->userService->update($data, auth()->user()), 201, false);
    }

    private function getRequestData()
    {
        return request(['name', 'email', 'password',
            'gender', 'birthday',]);
    }

    private function makeValidator($data)
    {
        $rules = [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'gender' => array('required', 'regex:/^(male|female)$/'),
            'birthday' => 'required|date_format:Y-m-d'
        ];
        return Validator::make($data, $rules);
    }


}
