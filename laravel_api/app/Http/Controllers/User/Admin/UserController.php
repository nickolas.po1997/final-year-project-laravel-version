<?php


namespace App\Http\Controllers\User\Admin;


use App\Facade\JsonResponse;
use App\Facade\Validate;
use App\Service\UserService\UserService;
use App\Service\ValidateService\ValidateService;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class UserController
{

    private $baseRule = [
        'password' => 'required|min:8',
        'gender' => array('required', 'regex:/^(male|female)$/'),
        'birthday' => 'required|date_format:Y-m-d',
        'roles' => 'array',
        'roles.*' => 'required|numeric|exists:roles,id',
    ];
    private $uniqueField = ['name', 'email'];

    private $userService;
    private $validateService;

    public function __construct(UserService $userService, ValidateService $validateService)
    {
        $this->userService = $userService;
        $this->validateService = $validateService;
    }

    public function listAll()
    {
        return JsonResponse::response($this->userService->findAllWithPageable(), 200, false);
    }

    public function view(User $user)
    {
        return JsonResponse::response($this->userService->viewInfo($user), 200, false);
    }

    public function create()
    {
        $additionRule = [
            'name' => 'required|min:3|unique:users',
            'email' => 'required|email|unique:users',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $additionRule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $target = $this->userService->create($data);
        return JsonResponse::response($this->userService->viewInfo($target), 201, false);
    }

    public function update(User $user)
    {
        $additionRule = [
            'name' => 'required|min:3',
            'email' => 'required|email',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $additionRule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        $customValidation = $this->validateService->validate($data, $this->userService, $user);
        if ($customValidation->count() > 0) {
            return JsonResponse::error($customValidation, 422, false);
        }
        return JsonResponse::response($this->userService->update($data, $user), 201, false);
    }

    public function delete(User $user)
    {
        return JsonResponse::response($this->userService->delete($user), 200, false);
    }

    private function getRequestData()
    {
        return request(['name', 'email', 'password',
            'gender', 'birthday', 'roles']);
    }

    private function makeValidator($data, $additionRule)
    {
        $rule = Arr::collapse([$additionRule, $this->baseRule]);
        return Validator::make($data, $rule);
    }

}
