<?php


namespace App\Http\Controllers\Tag;

use App\Facade\JsonResponse;
use App\Facade\Validate;
use App\Http\Controllers\Controller;
use App\Service\TagService\TagService;
use App\Tag;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class TagController extends Controller
{

    private $tagService;

    public function __construct(TagService $tagService)
    {
        $this->tagService = $tagService;
    }

    public function listAll()
    {
        return JsonResponse::response(
            $this->tagService->findAllWithPageable(), 200, false
        );
    }

    public function view(Tag $tag)
    {
        return JsonResponse::response($this->tagService->viewInfo($tag), 200, false);
    }

    public function search()
    {
        $data = Arr::get($this->getRequestData(), 'name');
        return JsonResponse::response($this->tagService->search($data), 200, false);
    }

    public function create()
    {
        $rule = [
            'name' => 'required|min:3|unique:tags',
        ];
        $data = $this->getRequestData();
        $validation = $this->makeValidator($data, $rule);
        if ($validation->fails()) {
            return JsonResponse::error($validation->errors(), 422, false);
        }
        return JsonResponse::response($this->tagService->create($data), 200, false);
    }

    public function delete(Tag $tag)
    {
        return JsonResponse::response($this->tagService->delete($tag), 200, false);
    }

    private function getRequestData()
    {
        return request(['name']);
    }

    private function makeValidator($data, $rule)
    {
        return Validator::make($data, $rule);
    }

}
