<?php


namespace App\Service\ImageService;


interface ImageService
{
    public function save();

    public function delete($target);
}
