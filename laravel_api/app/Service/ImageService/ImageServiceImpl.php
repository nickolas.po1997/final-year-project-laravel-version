<?php


namespace App\Service\ImageService;


use App\Repository\ImageRepository\ImageRepository;
use Illuminate\Support\Facades\Storage;

class ImageServiceImpl implements ImageService
{

    private $imageFolder = '/images';
    private $imageRepository;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    public function save()
    {
        $uploadedFiles = array();
        foreach (request()->file('images') as $image) {
            $path = $image->store($this->imageFolder, 'uploads');
            $url = url($path);
            $uploadedFiles[] = $this->imageRepository->save(compact('path', 'url'));
        }
        return compact('uploadedFiles');
    }

    public function delete($target)
    {
        Storage::disk('uploads')->delete($target->path);
        $isDeleted = $this->imageRepository->delete($target);
        return compact('isDeleted');
    }
}
