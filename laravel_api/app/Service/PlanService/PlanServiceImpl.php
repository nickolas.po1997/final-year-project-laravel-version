<?php


namespace App\Service\PlanService;


use App\Repository\ItineraryRepository\ItineraryRepository;
use App\Repository\PlanRepository\PlanRepository;
use Illuminate\Support\Arr;

class PlanServiceImpl implements PlanService
{

    private $planRepository;
    private $itineraryRepository;

//    private $userRepository;

    public function __construct(PlanRepository $planRepository, ItineraryRepository $itineraryRepository)
    {
        $this->planRepository = $planRepository;
        $this->itineraryRepository = $itineraryRepository;
    }

    public function viewInfo($target)
    {
        $plan = $this->planRepository->loadTarget($target);
        $itineraries = $this->itineraryRepository->list($target);
        return compact("plan", 'itineraries');
    }

    public function findAllWithPageable()
    {
        $plans = $this->planRepository->findAllWithPageable();
        return compact("plans");
    }

    public function create($data)
    {
        $plan = $this->planRepository->create($data, auth()->user());
        $this->syncRelationship($plan, $data);
        return $this->viewInfo($plan);
    }

    public function update($data, $target)
    {
        $planUpdate = $this->planRepository->update($data, $target);
        $this->syncRelationship($target, $data);
        return $this->viewInfo($target);
    }

    public function delete($target)
    {
        $isDeleted = $this->planRepository->delete($target);
        return compact('isDeleted');
    }

    private function syncRelationship($target, $data)
    {
        $tag_id_list = collect(Arr::get($data, 'tag_list'));
        $target->tags()->sync($tag_id_list);
    }

}
