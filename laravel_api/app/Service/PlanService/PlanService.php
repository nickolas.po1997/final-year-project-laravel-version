<?php


namespace App\Service\PlanService;


interface PlanService
{
    public function viewInfo($target);

    public function findAllWithPageable();

    public function create($data);

    public function update($data, $target);

    public function delete($target);
}

