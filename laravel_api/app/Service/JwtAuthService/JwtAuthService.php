<?php


namespace App\Service\JwtAuthService;


interface JwtAuthService
{
    public function login($data);

    public function register($data);

    public function logout();
}
