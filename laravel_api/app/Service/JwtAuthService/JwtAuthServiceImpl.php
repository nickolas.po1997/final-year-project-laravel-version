<?php


namespace App\Service\JwtAuthService;


use App\Facade\JsonResponse;
use App\Service\UserService\UserService;

class JwtAuthServiceImpl implements JwtAuthService
{

    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function login($data)
    {
        if (!$token = auth('api')->attempt($data)) {
            return JsonResponse::error("Unauthorized", 401, false);
        } else {
            return JsonResponse::response($token, 200, true);
        }
    }

    public function register($data)
    {
        $user = $this->userService->create($data);
        $token = auth()->login($user);
        return JsonResponse::response($token, 200, true);
    }

    public function logout()
    {
        auth('api')->logout();
        return JsonResponse::response(null, 200, false);
    }

    public function me()
    {
        $user = auth()->user();
        return JsonResponse::response(compact('user'), 200, false);
    }


}
