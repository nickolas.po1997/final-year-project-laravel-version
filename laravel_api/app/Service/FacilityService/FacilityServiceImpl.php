<?php


namespace App\Service\FacilityService;

use App\Repository\FacilityRepository\FacilityRepository;

class FacilityServiceImpl implements FacilityService
{

    protected $facilityRepository;

    public function __construct(FacilityRepository $facilityRepository)
    {
        $this->facilityRepository = $facilityRepository;
    }

    public function viewInfo($target)
    {
        $facility = $target;
        return compact("facility");
    }

    public function findByFieldValuePair($field, $value)
    {
        return $this->facilityRepository->findByFieldValuePair($field, $value);
    }

    public function findAllWithPageable()
    {
        $facilities = $this->facilityRepository->findAllWithPageable();
        return compact('facilities');
    }

    public function create($data)
    {
        $facility = $this->facilityRepository->create($data);
        return $this->viewInfo($facility);
    }

    public function update($data, $target)
    {
        $this->facilityRepository->update($data, $target);
        return $this->viewInfo($target);
    }

    public function delete($target)
    {
        $isDeleted = $this->facilityRepository->delete($target);
        return compact('isDeleted');
    }

}
