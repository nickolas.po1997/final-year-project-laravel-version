<?php


namespace App\Service\FacilityService;


interface FacilityService
{
    public function findAllWithPageable();

    public function findByFieldValuePair($field, $value);

    public function viewInfo($target);

    public function create($data);

    public function update($data, $target);

    public function delete($target);

}
