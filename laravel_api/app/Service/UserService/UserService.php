<?php


namespace App\Service\UserService;


interface UserService
{
    public function viewInfo($target);

    public function findAllWithPageable();

    public function findByFieldValuePair($field, $value);

    public function findByQuery($query);

    public function create($data);

    public function update($data,$target);

    public function delete($target);
}
