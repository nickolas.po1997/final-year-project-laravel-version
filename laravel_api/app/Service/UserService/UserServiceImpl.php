<?php


namespace App\Service\UserService;


use App\Repository\RoleRepository\RoleRepository;
use App\Repository\UserRepository\UserRepository;
use Illuminate\Support\Arr;

class UserServiceImpl implements UserService
{

    private $userRepository;
    private $roleRepository;

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    public function findAllWithPageable()
    {
        $users = $this->userRepository->findAllWithPageable();
        return compact('users');
    }

    public function findByFieldValuePair($field, $value)
    {
        return $this->userRepository->findByFieldValuePair($field, $value);
    }

    public function findByQuery($query)
    {
        return $this->findByQuery($query);
    }

    public function viewInfo($target)
    {
        $user = $this->userRepository->loadTarget($target);
        return compact('user');
    }

    public function create($data)
    {
        $data['password'] = bcrypt($data['password']);
        $user = $this->userRepository->create($data);
        $this->attachRoles($user, $data);
        return $user;
    }

    public function update($data, $target)
    {
        $data['password'] = bcrypt($data['password']);
        $this->userRepository->update($data, $target);
        $this->attachRoles($target, $data);
        return $this->viewInfo($target);
    }

    public function delete($target)
    {
        $isDeleted = $this->userRepository->delete($target);
        return compact('isDeleted');
    }

    private function attachRoles($target, $data)
    {
        $roles = Arr::get($data, 'role');
        if ($roles == null) {
            $target->roles()->sync($this->roleRepository->findByName('member'));
        } else {
            $target->roles()->sync($roles);
        }
    }

}
