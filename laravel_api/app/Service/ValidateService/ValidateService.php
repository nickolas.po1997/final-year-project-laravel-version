<?php


namespace App\Service\ValidateService;


interface ValidateService
{
    public function validate($data, $service, $target);
}
