<?php


namespace App\Service\ValidateService;


use App\Repository\ImageRepository\ImageRepository;
use App\Repository\ItineraryRepository\ItineraryRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\MessageBag;

class ValidateServiceImpl implements ValidateService
{

    private $uniqueField = ['name', 'email'];
    private $imageRepository;
    private $itineraryRepository;

    public function __construct(ImageRepository $imageRepository, ItineraryRepository $itineraryRepository)
    {
        $this->imageRepository = $imageRepository;
        $this->itineraryRepository = $itineraryRepository;
    }

    public function validate($data, $service, $target)
    {
        $message = new MessageBag();
        $message = $this->checkUniqueField($data, $service, $target, $message);
        $message = $this->checkImage($data, $message);
        $message = $this->checkIsInPlan($data, $target, $message);
        return $message;
    }

    private function checkUniqueField($data, $service, $target, $message)
    {
        $checkList = Arr::only($data, $this->uniqueField);
        if ($target != null) {
            foreach ($checkList as $field => $value) {
                if ($target->getAttribute($field) != $value) {
                    $checker = $service->findByFieldValuePair($field, $value);
                    if (!$checker->isEmpty()) {
                        $message->add($field, 'The ' . $field . ' has already been taken.');
                    }
                }
            }
        }
        return $message;
    }

    private function checkImage($data, $message)
    {
        $image_id_list = collect(Arr::get($data, 'image_list'));
        foreach ($image_id_list as $index => $id) {
            $target = $this->imageRepository->findById($id);
            if ($target->hasRelationship()) {
                $message->add("image_list." . $index . "",
                    'The selected image_list.' . $index . ' has already been taken.');
            }
        }
        return $message;
    }

    private function checkIsInPlan($data, $target, $message)
    {
        $order_list = collect(Arr::get($data, 'order_list'));
        foreach ($order_list as $index => $id) {
            if (!$target->itineraries->contains($id)) {
                $message->add("order_list." . $index . "",
                    'The selected order_list.' . $index . ' is not in this plan.');
            }
        }
        return $message;
    }

}
