<?php


namespace App\Service\RestaurantService;


use App\Repository\ImageRepository\ImageRepository;
use App\Repository\RestaurantRepository\RestaurantRepository;
use Illuminate\Support\Arr;

class RestaurantServiceImpl implements RestaurantService
{

    private $restaurantRepository;
    private $imageRepository;

    public function __construct(RestaurantRepository $restaurantRepository, ImageRepository $imageRepository)
    {
        $this->restaurantRepository = $restaurantRepository;
        $this->imageRepository = $imageRepository;
    }

    public function viewInfo($target)
    {
        $restaurant = $this->restaurantRepository->loadTarget($target);
        return compact("restaurant");
    }

    public function findAllWithPageable()
    {
        $restaurants = $this->restaurantRepository->findAllWithPageable();
        return compact("restaurants");
    }

    public function findByFieldValuePair($field, $value)
    {
        return $this->restaurantRepository->findByFieldValuePair($field, $value);
    }

    public function create($data)
    {
        $restaurant = $this->restaurantRepository->create($data);
        $this->syncRelationship($restaurant, $data);
        return $this->viewInfo($restaurant);
    }

    public function update($data, $target)
    {
        $restaurantUpdate = $this->restaurantRepository->update($data, $target);
        $this->syncRelationship($target, $data);
        return $this->viewInfo($target);
    }

    public function delete($target)
    {
        $isDeleted = $this->restaurantRepository->delete($target);
        return compact('isDeleted');
    }

    private function syncRelationship($target, $data)
    {
        $image_id_list = collect(Arr::get($data, 'image_list'));
        $tag_id_list = collect(Arr::get($data, 'tag_list'));
        $new_image_list = $image_id_list->map(function ($id) {
            return $this->imageRepository->findById($id);
        });
        $target->images()->saveMany($new_image_list);
        $target->tags()->sync($tag_id_list);
    }


}
