<?php


namespace App\Service\RestaurantService;


interface RestaurantService
{

    public function viewInfo($target);

    public function findAllWithPageable();

    public function findByFieldValuePair($field, $value);

    public function create($data);

    public function update($data, $target);

    public function delete($target);

}
