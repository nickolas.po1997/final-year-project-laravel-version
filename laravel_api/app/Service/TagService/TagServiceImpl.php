<?php


namespace App\Service\TagService;


use App\Repository\TagRepository\TagRepository;

class TagServiceImpl implements TagService
{

    private $tagRepository;

    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    public function viewInfo($target)
    {
        $tag = $this->tagRepository->loadTarget($target);
        return compact('tag');
    }

    public function findAllWithPageable()
    {
        $tags = $this->tagRepository->findAllWithPageable();
        return compact("tags");
    }

    public function findByFieldValuePair($field, $value)
    {
        return $this->tagRepository->findByFieldValuePair($field, $value);
    }

    public function create($data)
    {
        $tag = $this->tagRepository->create($data);
        return compact('tag');
    }

    public function search($data)
    {
        $searchResult = $this->tagRepository->searchByName($data);
        return compact("searchResult");
    }

    public function delete($target)
    {
        $isDelete = $this->tagRepository->delete($target);
        return compact('isDelete');
    }


}
