<?php


namespace App\Service\TagService;


interface TagService
{
    public function viewInfo($target);

    public function findAllWithPageable();

    public function findByFieldValuePair($field, $value);

    public function create($data);

    public function search($data);

    public function delete($target);
}
