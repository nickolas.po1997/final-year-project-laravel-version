<?php


namespace App\Service\ItineraryService;


use App\Repository\AttractionRepository\AttractionRepository;
use App\Repository\HotelRepository\HotelRepository;
use App\Repository\ItineraryRepository\ItineraryRepository;
use App\Repository\RestaurantRepository\RestaurantRepository;
use App\Service\PlanService\PlanService;
use Illuminate\Support\Arr;

class ItineraryServiceImpl implements ItineraryService
{

    private $itineraryRepository;
    private $hotelRepository;
    private $attractionRepository;
    private $restaurantRepository;
    private $planService;

    public function __construct(ItineraryRepository $itineraryRepository, HotelRepository $hotelRepository,
                                AttractionRepository $attractionRepository, RestaurantRepository $restaurantRepository,
                                PlanService $planService)
    {
        $this->itineraryRepository = $itineraryRepository;
        $this->hotelRepository = $hotelRepository;
        $this->attractionRepository = $attractionRepository;
        $this->restaurantRepository = $restaurantRepository;
        $this->planService = $planService;
    }

    public function viewInfo($target)
    {
        $itinerary = $this->itineraryRepository->loadTarget($target);
        return compact('itinerary');
    }

    public function create($data, $plan)
    {
        $itineraryable = $this->findItineraryable($data);
        $order = $plan->itineraries()->count();
        $data = Arr::add($data, 'order', $order);
        $itinerary = $this->itineraryRepository->create($data, $plan, $itineraryable);
        return $this->planService->viewInfo($plan);
    }

    public function update($data, $target)
    {
        $itineraryUpdated = $this->itineraryRepository->update($data, $target);
        return $this->viewInfo($target);
    }

    public function order($data, $plan)
    {
        $order_list = Arr::get($data, 'order_list');
        foreach ($order_list as $order => $id) {
            $target = $this->itineraryRepository->findById($id);
            $this->itineraryRepository->update(compact('order'), $target);
        }
        return $this->planService->viewInfo($plan);
    }

    public function delete($target)
    {
        $isDeleted = $this->itineraryRepository->delete($target);
        return compact('isDeleted');
    }

    private function findItineraryable($data)
    {
        switch (Arr::get($data, 'itineraryable_type')) {
            case 'A':
                return $this->attractionRepository->findById(Arr::get($data, 'itineraryable_id'));
                break;
            case 'H':
                return $this->hotelRepository->findById(Arr::get($data, 'itineraryable_id'));
                break;
            case 'R':
                return $this->restaurantRepository->findById(Arr::get($data, 'itineraryable_id'));
                break;
        }
    }


}
