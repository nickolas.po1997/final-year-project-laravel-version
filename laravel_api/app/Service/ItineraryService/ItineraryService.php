<?php


namespace App\Service\ItineraryService;


interface ItineraryService
{
    public function viewInfo($target);

    public function create($data, $plan);

    public function update($data, $target);

    public function order($data, $plan);

    public function delete($target);
}
