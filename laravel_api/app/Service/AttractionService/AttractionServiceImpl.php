<?php


namespace App\Service\AttractionService;


use App\Repository\AttractionRepository\AttractionRepository;
use App\Repository\ImageRepository\ImageRepository;
use Illuminate\Support\Arr;

class AttractionServiceImpl implements AttractionService
{

    private $attractionRepository;
    private $imageRepository;

    public function __construct(AttractionRepository $attractionRepository, ImageRepository $imageRepository)
    {
        $this->attractionRepository = $attractionRepository;
        $this->imageRepository = $imageRepository;
    }

    public function viewInfo($target)
    {
        $attraction = $this->attractionRepository->loadTarget($target);
        return compact('attraction');
    }

    public function findAllWithPageable()
    {
        $attractions = $this->attractionRepository->findAllWithPageable();
        return compact('attractions');
    }

    public function findByFieldValuePair($field, $value)
    {
        return $this->attractionRepository->findByFieldValuePair($field, $value);
    }

    public function create($data)
    {
        $attraction = $this->attractionRepository->create($data);
        $this->syncRelationship($attraction, $data);
        return $this->viewInfo($attraction);
    }

    public function update($data, $target)
    {
        $attractionUpdate = $this->attractionRepository->update($data, $target);
        $this->syncRelationship($target, $data);
        return $this->viewInfo($target);
    }

    public function delete($target)
    {
        $isDeleted = $this->attractionRepository->delete($target);
        return compact('isDeleted');
    }

    private function syncRelationship($target, $data)
    {
        $image_id_list = collect(Arr::get($data, 'image_list'));
        $tag_id_list = collect(Arr::get($data, 'tag_list'));
        $new_image_list = $image_id_list->map(function ($id) {
            return $this->imageRepository->findById($id);
        });
        $target->images()->saveMany($new_image_list);
        $target->tags()->sync($tag_id_list);
    }
}
