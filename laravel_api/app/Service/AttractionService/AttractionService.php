<?php


namespace App\Service\AttractionService;


interface AttractionService
{

    public function viewInfo($target);

    public function findAllWithPageable();

    public function findByFieldValuePair($field, $value);

    public function create($data);

    public function update($data,$target);

    public function delete($target);

}
