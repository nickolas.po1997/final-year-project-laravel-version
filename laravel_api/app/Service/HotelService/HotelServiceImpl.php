<?php


namespace App\Service\HotelService;


use App\Repository\FacilityRepository\FacilityRepository;
use App\Repository\HotelRepository\HotelRepository;
use App\Repository\ImageRepository\ImageRepository;
use Illuminate\Support\Arr;

class HotelServiceImpl implements HotelService
{

    protected $hotelRepository;
    protected $facilityRepository;
    protected $imageRepository;

    public function __construct(HotelRepository $hotelRepository, FacilityRepository $facilityRepository, ImageRepository $imageRepository)
    {
        $this->hotelRepository = $hotelRepository;
        $this->facilityRepository = $facilityRepository;
        $this->imageRepository = $imageRepository;
    }

    public function viewInfo($target)
    {
        $hotel = $this->hotelRepository->loadTarget($target);
        return compact("hotel");
    }

    public function findAllWithPageable()
    {
        $hotels = $this->hotelRepository->findAllWithPageable();
        return compact("hotels");
    }

    public function findByFieldValuePair($field, $value)
    {
        return $this->hotelRepository->findByFieldValuePair($field, $value);
    }

    public function create($data)
    {
        $hotel = $this->hotelRepository->create($data);
        $this->syncRelationship($hotel, $data);
        return $this->viewInfo($hotel);
    }

    public function update($data, $target)
    {
        $hotelUpdate = $this->hotelRepository->update($data, $target);
        $this->syncRelationship($target, $data);
        return $this->viewInfo($target);
    }

    public function delete($target)
    {
        $isDeleted = $this->hotelRepository->delete($target);
        return compact('isDeleted');
    }

    private function syncRelationship($target, $data)
    {
        $image_id_list = collect(Arr::get($data, 'image_list'));
        $facility_id_list = collect(Arr::get($data, 'facility_list'));
        $tag_id_list = collect(Arr::get($data, 'tag_list'));
        $target->facilities()->sync($facility_id_list);
        $new_image_list = $image_id_list->map(function ($id) {
            return $this->imageRepository->findById($id);
        });
        $target->images()->saveMany($new_image_list);
        $target->tags()->sync($tag_id_list);
    }


}
