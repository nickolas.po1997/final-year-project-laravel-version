<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{

    use SoftDeletes;

    protected $fillable = ['name', 'min_price', 'max_price', 'location',
        'description', 'website', 'rating',
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function facilities()
    {
        return $this->belongsToMany(Facility::class);
    }

    public function itineraries()
    {
        return $this->morphMany(Itinerary::class, 'itineraryable');
    }

}
