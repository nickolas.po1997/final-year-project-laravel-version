<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itinerary extends Model
{

    protected $fillable = ['description', 'order'];
    protected $hidden = ['itineraryable_id','itineraryable_type'];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function itineraryable()
    {
        return $this->morphTo();
    }

}
