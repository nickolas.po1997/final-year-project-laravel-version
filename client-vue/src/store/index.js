import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex)

const url = 'http://localhost:8000/api';

export default new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null,
        user: localStorage.getItem('login_user') || null
    },
    getters: {
        isLogged(status) {
            return status.token != null
        }
    },
    mutations: {
        retrieveToken(state, token) {
            state.token = token
        },
        destroyToken(state) {
            state.token = null
        }
    },
    actions: {
        login(context, loginForm) {
            return new Promise((resolve, reject) => {
                axios.post(`${url}/auth/login`, loginForm).then(response => {
                    localStorage.setItem('access_token', response.data.token)
                    context.commit('retrieveToken', response.data.token)
                    resolve(response)
                }).catch(error => {
                    reject(error)
                })
            })
        },
        register(context, registerForm) {
            return new Promise((resolve, reject) => {
                axios.post(`${url}/auth/register`, registerForm).then(response => {
                    localStorage.setItem('access_token', response.data.token)
                    context.commit('retrieveToken', response.data.token)
                    resolve(response)
                }).catch(error => {
                    console.log(error)
                    reject(error)
                })
            })
        },
        viewUser(context){
            axios.defaults.headers.common['Authorization'] = "Bearer " + context.state.token
            return new Promise((resolve, reject) => {
                axios.get(`${url}/user/view`).then(response => {
                    resolve(response)
                }).catch(error => {
                    console.log(error)
                    reject(error)
                })
            })
        },
        createPlan(context,createForm){
            axios.defaults.headers.common['Authorization'] = "Bearer " + context.state.token
            return new Promise((resolve, reject) => {
                axios.post(`${url}/plan/create`, createForm).then(response => {
                    resolve(response)
                }).catch(error => {
                    console.log(error)
                    reject(error)
                })
            })
        },
        listPlan(context,page){
            axios.defaults.headers.common['Authorization'] = "Bearer " + context.state.token
            // console.log(axios.defaults.headers.common['Authorization'])
            if (context.getters.isLogged) {
                return new Promise((resolve, reject) => {
                    axios.get(`${url}/plan/list/all`, {
                        params: {
                            page: page
                        }
                    }).then(response => {
                        resolve(response)
                    }).catch(error => {
                        reject(error)
                    })
                })
            }
        },
        viewPlan(context,planId){
            axios.defaults.headers.common['Authorization'] = "Bearer " + context.state.token
            // console.log(axios.defaults.headers.common['Authorization'])
            if (context.getters.isLogged) {
                return new Promise((resolve, reject) => {
                    axios.get(`${url}/plan/view/${planId}`).then(response => {
                        resolve(response)
                    }).catch(error => {
                        reject(error)
                    })
                })
            }
        },
        logout(context) {
            axios.defaults.headers.common['Authorization'] = "Bearer " + context.state.token
            // console.log(axios.defaults.headers.common['Authorization'])
            if (context.getters.isLogged) {
                return new Promise((resolve, reject) => {
                    axios.get(`${url}/auth/logout`).then(response => {
                        localStorage.removeItem('access_token')
                        context.commit('destroyToken')
                        resolve(response)
                    }).catch(error => {
                        localStorage.removeItem('access_token')
                        context.commit('destroyToken')
                        reject(error)
                    })
                })
            }
        }
    },
    modules: {}
})
