import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import store from './store'
import router from './router'
import ApiRequest from "./service/ApiRequest";

Vue.config.productionTip = false
Vue.prototype.$apiRequest = new ApiRequest()

new Vue({
    vuetify,
    store,
    router,
    render: h => h(App)
}).$mount('#app')
