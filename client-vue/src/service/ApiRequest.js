import axios from 'axios';

const url = 'http://localhost:8000/api';

class ApiRequest {

    listHotel(page) {
        return new Promise((resolve, reject) => {
            axios.get(`${url}/hotel/list/all`, {
                params: {
                    page: page
                }
            }).then(response => {
                resolve(response)
            }).catch(error => {
                reject(error)
            })
        })
    }

    listAttraction(page) {
        return new Promise((resolve, reject) => {
            axios.get(`${url}/attraction/list/all?page=${page}`).then(response => {
                resolve(response)
            }).catch(error => {
                reject(error)
            })
        })
    }

    listRestaurant(page) {
        return new Promise((resolve, reject) => {
            axios.get(`${url}/restaurant/list/all?page=${page}`).then(response => {
                resolve(response)
            }).catch(error => {
                reject(error)
            })
        })
    }

    viewHotel(hotelId) {
        return new Promise((resolve, reject) => {
            axios.get(`${url}/hotel/view/${hotelId}`).then(response => {
                resolve(response)
            }).catch(error => {
                reject(error)
            })
        })
    }

    viewAttraction(attractionId) {
        return new Promise((resolve, reject) => {
            axios.get(`${url}/attraction/view/${attractionId}`).then(response => {
                resolve(response)
            }).catch(error => {
                reject(error)
            })
        })
    }

    viewRestaurant(restaurantId) {
        return new Promise((resolve, reject) => {
            axios.get(`${url}/restaurant/view/${restaurantId}`).then(response => {
                resolve(response)
            }).catch(error => {
                reject(error)
            })
        })
    }



}

export default ApiRequest