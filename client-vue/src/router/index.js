import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Hotel from "../views/Hotel";
import Attraction from "../views/Attraction";
import Restaurant from "../views/Restaurant";
import Plan from "../views/Plan";
import Register from "../views/Register";
import Profile from "../views/Profile";
import CreatePlan from "../views/CreatePlan";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/hotel',
        name: 'Hotel',
        component: Hotel
    },
    {
        path: '/attraction',
        name: 'Attraction',
        component: Attraction
    },
    {
        path: '/restaurant',
        name: 'Restaurant',
        component: Restaurant
    },
    {
        path: '/plan',
        name: 'Plan',
        component: Plan
    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile
    },
    {
        path: '/plan/create',
        name: 'Create Plan',
        component: CreatePlan
    },
]

const router = new VueRouter({
    mode: 'history',
    routes,
})

export default router
